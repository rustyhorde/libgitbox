# libgitbox
git-box clean and smudge filter library

## Status
[![Build Status](https://travis-ci.org/rustyhorde/libgitbox.svg?branch=stable)](travis)
[![Build status](https://ci.appveyor.com/api/projects/status/4b53ux44adlcyc11?svg=true)](https://ci.appveyor.com/project/CraZySacX/libgitbox)
[![Latest Version](https://img.shields.io/crates/v/libgitbox.svg)](https://crates.io/crates/libgitbox)
