use boxstorage::*;
use git::*;
use std::io::{BufWriter, Read, Write};
use std::net::TcpStream;
use std::path::PathBuf;
use ssh2::Session;

pub fn config_ssh_storage(env: BoxStorageEnv) -> Result<(), BoxStorageError> {
    let ref path = env.sshpath;
    let ref host = env.sshhost;
    let ref user = env.sshuser;

    try!(set_config("box.storagetype", "SSH", env.global));
    try!(set_config("box.ssh.host", &host[..], env.global));
    try!(set_config("box.ssh.user", &user[..], env.global));
    try!(set_config("box.ssh.path", &path[..], env.global));

    if env.verbose {
        debug!("Set git-box SSH config:");
        debug!("  box.storagetype: {}", "SSH");
        debug!("  box.ssh.user:    {}", user);
        debug!("  box.ssh.host:    {}", host);
        debug!("  box.ssh.path:    {}", path);
    }

    Ok(())
}

pub fn unconfig_ssh_storage(env: BoxStorageEnv) -> Result<(), BoxStorageError> {
    try!(unset_config("box.storagetype", env.global));
    try!(unset_config("box.ssh.host", env.global));
    try!(unset_config("box.ssh.user", env.global));
    try!(unset_config("box.ssh.path", env.global));

    if env.verbose {
        debug!("Unset git-box SSH config");
    }

    Ok(())
}

pub struct Ssh;

impl Clean for Ssh {
    fn clean(&self, buf: &mut Vec<u8>, sha1: &str, size: usize) -> BoxStorageResult {
        let ssh_host = try!(ssh_host());
        let ssh_user = try!(ssh_user());
        let ssh_path = try!(ssh_path());

        let hostport = format!("{}:{}", ssh_host, 22);
        let tcp = try!(TcpStream::connect(&hostport[..]));
        let mut sess = Session::new().unwrap();
        try!(sess.handshake(&tcp));
        try!(sess.userauth_agent(&ssh_user[..]));

        let mut path = PathBuf::from(&ssh_path[..]);
        path.push(sha1);

        let sftp = try!(sess.sftp());
        let file = try!(sftp.create(path.as_path()));

        let mut bufw = BufWriter::new(file);
        try!(bufw.write_all(&buf[..]));
        try!(bufw.flush());
        Ok(size)
    }
}

impl Smudge for Ssh {
    fn smudge(&self, buf: &mut Vec<u8>, sha1: &str, _size: usize) -> BoxStorageResult {
        let ssh_host = try!(ssh_host());
        let ssh_user = try!(ssh_user());
        let ssh_path = try!(ssh_path());

        let hostport = format!("{}:{}", ssh_host, 22);
        let tcp = try!(TcpStream::connect(&hostport[..]));
        let mut sess = Session::new().unwrap();
        try!(sess.handshake(&tcp));
        try!(sess.userauth_agent(&ssh_user[..]));

        let mut path = PathBuf::from(&ssh_path[..]);
        path.push(sha1);

        let sftp = try!(sess.sftp());
        let mut remote_file = try!(sftp.open(path.as_path()));
        Ok(try!(remote_file.read_to_end(buf)))
    }
}

fn ssh_host() -> Result<String, BoxStorageError> {
    Ok(try!(get_config("box.ssh.host")))
}

fn ssh_user() -> Result<String, BoxStorageError> {
    Ok(try!(get_config("box.ssh.user")))
}

fn ssh_path() -> Result<String, BoxStorageError> {
    Ok(try!(get_config("box.ssh.path")))
}
