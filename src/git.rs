use boxstorage::BoxStorageError;
use std::process::Command;

fn add_global(cmd: &mut Command, global: bool) {
    if global {
        cmd.arg("--global");
    }
}

pub fn git_repo_path() -> Command {
    let mut config = Command::new("git");
    config.arg("rev-parse");
    config.arg("--show-toplevel");
    config
}

pub fn git_config() -> Command {
    let mut config = Command::new("git");
    config.arg("config");
    config
}

pub fn set_config(key: &str, val: &str, g: bool) -> Result<(), BoxStorageError> {
    let mut cfg = git_config();
    add_global(&mut cfg, g);
    cfg.arg(key);
    cfg.arg(val);

    try!(cfg.output());
    Ok(())
}

pub fn unset_config(key: &str, g: bool) -> Result<(), BoxStorageError> {
    let mut cfg = git_config();
    add_global(&mut cfg, g);
    cfg.arg("--unset");
    cfg.arg(key);

    try!(cfg.output());
    Ok(())
}

pub fn get_config(key: &str) -> Result<String, BoxStorageError> {
    let mut cfg = git_config();
    cfg.arg("--get");
    cfg.arg(key);

    let output = try!(cfg.output());
    let untrimmed = try!(String::from_utf8(output.stdout));

    Ok(untrimmed.trim().to_owned())
}

pub fn get_repo_path() -> Result<String, BoxStorageError> {
    let mut rpc = git_repo_path();
    let output = try!(rpc.output());
    let untrimmed = try!(String::from_utf8(output.stdout));
    Ok(untrimmed.trim().to_owned())
}
