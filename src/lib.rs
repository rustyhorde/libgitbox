#![cfg_attr(feature = "clippy", feature(plugin))]
#![cfg_attr(feature = "clippy", plugin(clippy))]
#![cfg_attr(feature = "clippy", deny(clippy, clippy_pedantic))]
#[cfg(feature = "artifactory")]
extern crate curl;
#[macro_use]
extern crate log;
#[cfg(feature = "ssh")]
extern crate mush as ssh2;
extern crate rustc_serialize;
extern crate sha1;

pub use boxstorage::*;
pub use git::*;

#[cfg(feature = "artifactory")]
pub use art::{config_art_storage, unconfig_art_storage};
#[cfg(feature = "artifactory")]
use art::Art;
pub use file::{config_file_storage, unconfig_file_storage};
use file::Local;
use sha1::Sha1;
#[cfg(feature = "ssh")]
pub use ssh::{config_ssh_storage, unconfig_ssh_storage};
#[cfg(feature = "ssh")]
use ssh::Ssh;
use std::fs::OpenOptions;
use std::io::{self, BufWriter, Read, Write};
use std::str::FromStr;
pub use version::version;

#[cfg(feature = "artifactory")]
mod art;
mod boxstorage;
mod file;
mod git;
#[cfg(feature = "ssh")]mod ssh;
mod version;

pub fn config_filters(env: BoxStorageEnv) -> Result<(), BoxStorageError> {
    try!(set_config("filter.box.smudge", "git-box-smudge", env.global));
    try!(set_config("filter.box.clean", "git-box-clean", env.global));

    if env.verbose {
        debug!("Set git-box filter config:");
        debug!("  filter.box.smudge: git-box-smudge");
        debug!("  filter.box.clean:  git-box-clean");
    }

    Ok(())
}

pub fn unconfig_filters(env: BoxStorageEnv) -> Result<(), BoxStorageError> {
    try!(unset_config("filter.box.smudge", env.global));
    try!(unset_config("filter.box.clean", env.global));

    if env.verbose {
        debug!("Unset git-box filter config");
    }

    Ok(())
}

pub fn clean_filter() -> BoxStorageResult {
    let mut buf = Vec::new();

    try!(io::stdin().read_to_end(&mut buf));

    let mut m = Sha1::new();
    m.update(&buf[..]);
    let sha1 = m.hexdigest();
    let size = buf.len();

    let st = try!(storage_type());
    let res = match &st[..] {
        #[cfg(feature = "artifactory")]
        "ARTIFACTORY" => Art.clean(&mut buf, &sha1[..], size),
        "FILE" => Local.clean(&mut buf, &sha1[..], size),
        #[cfg(feature = "ssh")]
        "SSH" => Ssh.clean(&mut buf, &sha1[..], size),
        _ => Err(Other("Unknown storage type!")),
    };

    match res {
        Ok(_) => {
            let out = format!("{}${}\n", &sha1, size);
            let mut bufw = BufWriter::new(io::stdout());
            try!(bufw.write_all(out.as_bytes()));
            try!(bufw.flush());
            Ok(size)
        }
        Err(e) => {
            try!(writeln!(io::stderr(), "ERROR: {:?}", e));
            let mut bufw = BufWriter::new(io::stdout());
            try!(bufw.write_all(&buf[..]));
            try!(bufw.flush());
            Ok(size)
        }
    }
}

pub fn smudge_filter() -> BoxStorageResult {
    let mut buf = Vec::new();

    try!(io::stdin().read_to_end(&mut buf));

    let contents = try!(String::from_utf8(buf.clone()));
    let strs: Vec<&str> = contents.trim().split('$').collect();

    let sha1 = String::from(strs[0]);
    let size: usize = try!(FromStr::from_str(strs[1]));

    let mut outbuf = Vec::with_capacity(size);

    let st = try!(storage_type());
    let res = match &st[..] {
        #[cfg(feature = "artifactory")]
        "ARTIFACTORY" => Art.smudge(&mut outbuf, &sha1[..], size),
        "FILE" => Local.smudge(&mut outbuf, &sha1[..], size),
        #[cfg(feature = "ssh")]
        "SSH" => Ssh.smudge(&mut outbuf, &sha1[..], size),
        _ => Err(Other("Unknown storage type!")),
    };

    match res {
        Ok(_) => {
            let mut bufw = BufWriter::new(io::stdout());
            try!(bufw.write_all(&outbuf[..]));
            try!(bufw.flush());
            Ok(size)
        }
        Err(e) => {
            try!(writeln!(io::stderr(), "ERROR: {:?}", e));
            let mut bufw = BufWriter::new(io::stdout());
            try!(bufw.write_all(&buf[..]));
            try!(bufw.flush());
            Ok(size)
        }
    }
}

pub fn track(env: BoxStorageEnv) -> Result<(), BoxStorageError> {
    let fp = &env.pattern;
    if env.verbose {
        debug!("Adding {} to list of tracked file patterns", fp);
    }

    let mut gitattr = try!(OpenOptions::new()
                               .create(true)
                               .write(true)
                               .append(true)
                               .open(".gitattributes"));

    // Check if the filter already exists...

    let output = format!("{} filter=box\n", fp);
    let ob = output.as_bytes();

    try!(gitattr.write_all(&ob[..]));
    Ok(())
}

fn storage_type() -> Result<String, BoxStorageError> {
    let mut st = git_config();
    st.arg("--get");
    st.arg("box.storagetype");

    let out = try!(st.output());
    let stdout = try!(String::from_utf8(out.stdout));

    Ok(String::from(stdout.trim()))
}
