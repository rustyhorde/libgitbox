use boxstorage::*;
use git::*;
use sha1::Sha1;
use std::fs::{File, OpenOptions};
use std::io::{BufWriter, Read, Write};
use std::path::PathBuf;

pub fn config_file_storage(env: BoxStorageEnv) -> Result<(), BoxStorageError> {
    let path = &env.filepath;
    try!(set_config("box.storagetype", "FILE", env.global));
    try!(set_config("box.filepath", &path[..], env.global));

    if env.verbose {
        debug!("Set git-box FILE config:");
        debug!("  box.storagetype: {}", "FILE");
        debug!("  box.filepath:    {}", path);
    }

    Ok(())
}

pub fn unconfig_file_storage(env: BoxStorageEnv) -> Result<(), BoxStorageError> {
    try!(unset_config("box.storagetype", env.global));
    try!(unset_config("box.filepath", env.global));

    if env.verbose {
        debug!("Unset git-box FILE config");
    }

    Ok(())
}

pub struct Local;

impl Clean for Local {
    fn clean(&self, buf: &mut Vec<u8>, sha1: &str, size: usize) -> BoxStorageResult {
        // Check the buffer matches given sha1
        let mut m = Sha1::new();
        m.update(&buf[..]);
        assert!(sha1 == m.hexdigest());
        assert!(buf.len() == size);

        // Write the buffer to the filepath.
        let file_path = try!(get_config("box.filepath"));
        let mut path = PathBuf::from(file_path);
        path.push(sha1);

        let f = try!(OpenOptions::new()
                         .create(true)
                         .write(true)
                         .open(path));

        let mut bufw = BufWriter::new(f);
        try!(bufw.write_all(&buf[..]));
        try!(bufw.flush());

        Ok(size)
    }
}

impl Smudge for Local {
    fn smudge(&self, buf: &mut Vec<u8>, sha1: &str, size: usize) -> BoxStorageResult {
        let file_path = try!(get_config("box.filepath"));
        let mut path = PathBuf::from(file_path);
        path.push(sha1);

        let mut f = try!(File::open(path));

        let actual_size = try!(f.read_to_end(buf));
        assert!(size == actual_size);
        Ok(size)
    }
}
