use boxstorage::*;
use curl::http::{self, Response};
use git::*;
use rustc_serialize::base64::*;
use std::collections::HashSet;
use std::io::{self, Write};

pub fn config_art_storage(env: BoxStorageEnv) -> Result<(), BoxStorageError> {
    let user = &env.artuser;
    let pass = &env.artpass;
    let url = &env.arturl;
    let repo = &env.artrepo;
    let repodir = &env.artrepodir;

    try!(set_config("box.storagetype", "ARTIFACTORY", env.global));
    try!(set_config("box.artifactory.user", &user[..], env.global));
    try!(set_config("box.artifactory.pass", &pass[..], env.global));
    try!(set_config("box.artifactory.url", &url[..], env.global));
    try!(set_config("box.artifactory.repo", &repo[..], env.global));
    try!(set_config("box.artifactory.repodir", &repodir[..], env.global));

    if env.verbose {
        debug!("Set git-box ARTIFACTORY config:");
        debug!("  box.storagetype:         {}", "ARTIFACTORY");
        debug!("  box.artifactory.user:    {}", user);
        debug!("  box.artifactory.pass:    {}", pass);
        debug!("  box.artifactory.url:     {}", url);
        debug!("  box.artifactory.repo:    {}", repo);
        debug!("  box.artifactory.repodir: {}", repodir);
    }

    Ok(())
}

pub fn unconfig_art_storage(env: BoxStorageEnv) -> Result<(), BoxStorageError> {
    try!(unset_config("box.storagetype", env.global));
    try!(unset_config("box.artifactory.user", env.global));
    try!(unset_config("box.artifactory.pass", env.global));
    try!(unset_config("box.artifactory.url", env.global));
    try!(unset_config("box.artifactory.repo", env.global));
    try!(unset_config("box.artifactory.repodir", env.global));

    if env.verbose {
        debug!("Unset git-box ARTIFACTORY config");
    }

    Ok(())
}

#[derive(Default)]
struct ArtResponse {
    code: u32,
    body: Vec<u8>,
}

impl ArtResponse {
    fn get_code(&self) -> u32 {
        self.code
    }

    fn get_body(&self) -> &[u8] {
        &self.body
    }
}

impl From<Response> for ArtResponse {
    fn from(resp: Response) -> ArtResponse {
        ArtResponse {
            code: resp.get_code(),
            body: resp.move_body(),
        }
    }
}

pub type ArtResult<T> = Result<T, BoxStorageError>;

pub struct Art;

fn put(url: &str,
       buf: &[u8],
       headers: HashSet<(&str, &str)>,
       timeout: Option<usize>)
       -> ArtResult<ArtResponse> {
    let mut handle = http::handle();

    let resp = match timeout {
        Some(t) => {
            try!(handle.connect_timeout(t)
                       .timeout(t)
                       .put(&url[..], &buf[..])
                       .headers(headers.into_iter())
                       .exec())
        }
        None => try!(handle.put(&url[..], &buf[..]).headers(headers.into_iter()).exec()),
    };

    Ok(ArtResponse::from(resp))
}

fn get(url: &str,
       headers: HashSet<(&str, &str)>,
       timeout: Option<usize>)
       -> ArtResult<ArtResponse> {
    let mut handle = http::handle();

    let resp = match timeout {
        Some(t) => {
            try!(handle.connect_timeout(t)
                       .timeout(t)
                       .get(&url[..])
                       .headers(headers.into_iter())
                       .exec())
        }
        None => try!(handle.get(&url[..]).headers(headers.into_iter()).exec()),
    };

    Ok(ArtResponse::from(resp))
}

impl Clean for Art {
    fn clean(&self, buf: &mut Vec<u8>, sha1: &str, size: usize) -> BoxStorageResult {
        let art_user = try!(artifactory_user());
        let art_pass = try!(artifactory_pass());
        let art_url = try!(artifactory_url());
        let art_repo = try!(artifactory_repo());
        let art_repodir = try!(artifactory_repodir());
        let empty_buf = Vec::new();

        // Setup the Artifactory URL: <url> / <artrepo> / <gitrepo>
        let mut url = try!(base_url(art_url, art_repo, art_repodir));
        url.push('/');

        // Setup the base64 Basic Authorization string for headers
        let basic_auth = basic_auth(art_user, art_pass);
        let mut headers = HashSet::new();
        headers.insert(("Authorization", &basic_auth[..]));

        // Create the directory on artifactory.  If the directory already exists
        // this has no effect.
        let resp = try!(put(&url, &empty_buf, headers, None));

        match resp.get_code() {
            201 => {}
            _ => {
                try!(stderr_resp(resp));
                return Err(Other("Error creating artifactory directory!"));
            }
        }

        // Extend the URL.
        url.push_str(sha1);

        // Add the checksum deploy headers.
        let mut headers1 = HashSet::new();
        headers1.insert(("Authorization", &basic_auth[..]));
        headers1.insert(("X-Checksum-Deploy", "true"));
        headers1.insert(("X-Checksum-Sha1", sha1));

        // Put the file (checksum deploy)
        let resp1 = try!(put(&url, &empty_buf, headers1, None));

        match resp1.get_code() {
            201 => return Ok(size),
            404 => {
                // Try a normal deploy
            }
            _ => {
                try!(stderr_resp(resp1));
                return Err(Other("Error deploying artifact (checksum)!"));
            }
        }

        // Checksum deploy didn't work so remove header and to a normal deploy.
        let mut headers2 = HashSet::new();
        headers2.insert(("Authorization", &basic_auth[..]));
        headers2.insert(("X-Checksum-Sha1", sha1));

        // Put the file
        let resp2 = try!(put(&url, buf, headers2, Some(3600000)));

        match resp2.get_code() {
            201 => {
                // TODO: Parse the response for size and assert here.
                Ok(size)
            }
            _ => {
                try!(stderr_resp(resp2));
                Err(Other("Error creating artifact!"))
            }
        }
    }
}

impl Smudge for Art {
    fn smudge(&self, buf: &mut Vec<u8>, sha1: &str, size: usize) -> BoxStorageResult {
        let art_user = try!(artifactory_user());
        let art_pass = try!(artifactory_pass());
        let art_url = try!(artifactory_url());
        let art_repo = try!(artifactory_repo());
        let art_repodir = try!(artifactory_repodir());

        // Setup the Artifactory URL: <url> / <artrepo> / <gitrepo>
        let mut url = try!(base_url(art_url, art_repo, art_repodir));

        // Setup the base64 Basic Authorization string
        let basic_auth = basic_auth(art_user, art_pass);
        let mut headers = HashSet::new();
        headers.insert(("Authorization", &basic_auth[..]));

        // Extend the URL.
        url.push('/');
        url.push_str(sha1);

        // Get the file
        // Bump up the smudge timeout to account for download times
        let resp = try!(get(&url, headers, Some(3600000)));

        match resp.get_code() {
            200 => {
                let body = resp.get_body();

                // Copy the response body into the buffer
                for rb in body.iter() {
                    buf.push(*rb);
                }

                Ok(size)
            }
            _ => {
                try!(stderr_resp(resp));
                Err(Other("Error getting artifact!"))
            }
        }
    }
}

fn artifactory_user() -> Result<String, BoxStorageError> {
    Ok(try!(get_config("box.artifactory.user")))
}

fn artifactory_pass() -> Result<String, BoxStorageError> {
    Ok(try!(get_config("box.artifactory.pass")))
}

fn artifactory_url() -> Result<String, BoxStorageError> {
    Ok(try!(get_config("box.artifactory.url")))
}

fn artifactory_repo() -> Result<String, BoxStorageError> {
    Ok(try!(get_config("box.artifactory.repo")))
}

fn artifactory_repodir() -> Result<String, BoxStorageError> {
    Ok(try!(get_config("box.artifactory.repodir")))
}

fn basic_auth(user: String, pass: String) -> String {
    let mut auth = user;
    auth.push(':');
    auth.push_str(&pass[..]);

    let enc = auth.as_bytes().to_base64(STANDARD);
    let mut basic = String::from("Basic ");
    basic.push_str(&enc[..]);
    basic
}

fn base_url(host: String, repo: String, repodir: String) -> Result<String, BoxStorageError> {
    // Setup the Artifactory URL: <url> / <repo> / <repodir>
    let mut url = String::from(host[..].trim_matches('/'));
    url.push('/');
    url.push_str(&repo[..]);
    url.push('/');
    url.push_str(&repodir[..]);
    Ok(url)
}

fn stderr_resp(resp: ArtResponse) -> Result<(), BoxStorageError> {
    let body_vec = resp.get_body();
    let body = String::from_utf8_lossy(&body_vec[..]);
    try!(writeln!(io::stderr(), "HTTP RESPONSE CODE: {:?}", resp.get_code()));
    try!(writeln!(io::stderr(), "{:?}", body));
    Ok(())
}
