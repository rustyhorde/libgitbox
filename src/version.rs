include!(concat!(env!("OUT_DIR"), "/version.rs"));

#[cfg(unix)]
fn verbose_ver() -> String {
    format!("\x1b[32;1mlibgitbox {}\x1b[0m ({} {}) (built {})\ncommit-hash: {}\ncommit-date: \
             {}\nbuild-date: {}\nhost: {}\nrelease: {}",
            semver(),
            short_sha(),
            commit_date(),
            short_now(),
            sha(),
            commit_date(),
            short_now(),
            target(),
            semver())
}

#[cfg(windows)]
fn verbose_ver() -> String {
    format!("libgitbox {} ({} {}) (built {})\ncommit-hash: {}\ncommit-date: {}\nbuild-date: \
             {}\nhost: {}\nrelease: {}",
            semver(),
            short_sha(),
            commit_date(),
            short_now(),
            sha(),
            commit_date(),
            short_now(),
            target(),
            semver())
}

#[cfg(unix)]
fn ver() -> String {
    format!("\x1b[32;1mlibgitbox {}\x1b[0m ({} {}) (built {})",
            semver(),
            short_sha(),
            commit_date(),
            short_now())
}

#[cfg(windows)]
fn ver() -> String {
    format!("libgitbox {}[0m ({} {}) (built {})",
            semver(),
            short_sha(),
            commit_date(),
            short_now())
}

pub fn version(verbose: bool) -> String {
    if verbose {
        verbose_ver()
    } else {
        ver()
    }
}
