#[cfg(feature = "artifactory")]
use curl::ErrCode;
#[cfg(feature = "ssh")]
use ssh2::Error as Ssh2Error;
use std::default::Default;
use std::io;
use std::num::ParseIntError;
use std::string::FromUtf8Error;

pub use self::BoxStorageError::*;

#[derive(Debug)]
pub enum BoxStorageError {
    #[cfg(feature = "artifactory")]
    CURL(ErrCode),
    IO(io::Error),
    PARSEINT(ParseIntError),
    PARSESTR(FromUtf8Error),
    #[cfg(feature = "ssh")]
    SSH(Ssh2Error),
    Other(&'static str),
}

#[cfg(feature = "artifactory")]
impl From<ErrCode> for BoxStorageError {
    fn from(err: ErrCode) -> BoxStorageError {
        CURL(err)
    }
}

impl From<io::Error> for BoxStorageError {
    fn from(err: io::Error) -> BoxStorageError {
        IO(err)
    }
}

#[cfg(feature = "ssh")]
impl From<Ssh2Error> for BoxStorageError {
    fn from(err: Ssh2Error) -> BoxStorageError {
        SSH(err)
    }
}

impl From<ParseIntError> for BoxStorageError {
    fn from(err: ParseIntError) -> BoxStorageError {
        PARSEINT(err)
    }
}

impl From<FromUtf8Error> for BoxStorageError {
    fn from(err: FromUtf8Error) -> BoxStorageError {
        PARSESTR(err)
    }
}

pub type BoxStorageResult = Result<usize, BoxStorageError>;

pub trait Clean {
    fn clean(&self, buf: &mut Vec<u8>, sha1: &str, size: usize) -> BoxStorageResult;
}

pub trait Smudge {
    fn smudge(&self, buf: &mut Vec<u8>, sha1: &str, size: usize) -> BoxStorageResult;
}

#[derive(Default)]
pub struct BoxStorageEnv {
    pub global: bool,
    pub verbose: bool,

    // track config
    pub pattern: String,

    // file config
    pub filepath: String,

    // ssh config
    pub sshhost: String,
    pub sshpath: String,
    pub sshuser: String,

    // Artifactoryconfig
    pub artuser: String,
    pub artpass: String,
    pub arturl: String,
    pub artrepo: String,
    pub artrepodir: String,
}

impl BoxStorageEnv {
    pub fn new() -> BoxStorageEnv {
        Default::default()
    }

    pub fn global(&mut self, global: bool) -> &mut BoxStorageEnv {
        self.global = global;
        self
    }

    pub fn verbose(&mut self, verbose: bool) -> &mut BoxStorageEnv {
        self.verbose = verbose;
        self
    }

    pub fn pattern(&mut self, pattern: String) -> &mut BoxStorageEnv {
        self.pattern = pattern;
        self
    }

    pub fn filepath(&mut self, filepath: String) -> &mut BoxStorageEnv {
        self.filepath = filepath;
        self
    }

    pub fn sshpath(&mut self, sshpath: String) -> &mut BoxStorageEnv {
        self.sshpath = sshpath;
        self
    }

    pub fn sshhost(&mut self, sshhost: String) -> &mut BoxStorageEnv {
        self.sshhost = sshhost;
        self
    }

    pub fn sshuser(&mut self, sshuser: String) -> &mut BoxStorageEnv {
        self.sshuser = sshuser;
        self
    }

    pub fn artuser(&mut self, artuser: String) -> &mut BoxStorageEnv {
        self.artuser = artuser;
        self
    }

    pub fn artpass(&mut self, artpass: String) -> &mut BoxStorageEnv {
        self.artpass = artpass;
        self
    }

    pub fn arturl(&mut self, arturl: String) -> &mut BoxStorageEnv {
        self.arturl = arturl;
        self
    }

    pub fn artrepo(&mut self, artrepo: String) -> &mut BoxStorageEnv {
        self.artrepo = artrepo;
        self
    }

    pub fn artrepodir(&mut self, artrepodir: String) -> &mut BoxStorageEnv {
        self.artrepodir = artrepodir;
        self
    }
}
